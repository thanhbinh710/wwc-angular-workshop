import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message = 'My app name is superheroes app';
  imageUrl = 'https://i.pinimg.com/originals/29/df/48/29df48fb6b4f90029b1916a0d513691b.png';
  correctResult: boolean;

  resetMessage(): void {
    this.message = 'Batman';
  }

  checkName(name) {
    if (name && name === 'Bruce Wayne') {
      this.correctResult = true;
    } else {
      this.correctResult = false;
    }
  }
}
