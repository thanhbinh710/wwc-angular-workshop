import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  @Input() heroName: string;
  @Output() checkHeroName = new EventEmitter();
  realName: string;

  constructor() { }

  ngOnInit() {
  }

  checkName() {
    this.checkHeroName.emit(this.realName);
  }

}
